Editar XHTML amb un editor estructurat
======================================

MP4UF1A3T4

Estudi de les característiques d’un editor d’HTML estructurat

Edició d’XML
------------

Coses que es poden demanar a un editor estructurat d’XML:

-   Oferir elements/atributs a inserir segons DTD.
-   Validació del document.
-   Agrupació del codi en blocs, sintaxi colorejada.
-   Autocompletar etiquetes de tancament dels elements.
-   Posar cometes als valors dels atributs.
-   etc.

L’editor XML Copy Editor, distribuït com a paquet de Fedora, satisfà
tots els requeriments exigibles a un editor estructurat per XML.

Enllaços recomanats
-------------------

-   [Wikipedia: Structured
    editor](http://en.wikipedia.org/wiki/Structure_editor)
-   [XML Copy Editor](http://xml-copy-editor.sourceforge.net/)

Us de simple text amb convencions estructurals
----------------------------------------------

Una alternativa pels autors que han de crear documents XHTML o DocBook i
volen concentrar-se en els continguts i no haver de pensar en el marcat
són els sistemes fonamentats en crear simple text però amb convencions
estructurals. Alguns dels més coneguts són aquests:

-   [Markdown](http://daringfireball.net/projects/markdown/syntax)
-   [AsciiDoc](http://www.methods.co.nz/asciidoc/)
-   [reStructuredText](http://docutils.sourceforge.net/rst.html)
-   [Pandoc](http://johnmacfarlane.net/pandoc/README.html)

El més senzill i popular d’aquests sistemes és Markdown, que es pot
resumir en una sola pàgina com ara aquestes:

-   [packetlife.net](http://packetlife.net/media/library/16/Markdown.pdf)
    ([local](aux/Markdown-1.pdf))
-   [Mark
    Boszko](http://stationinthemetro.com/wp-content/uploads/2013/04/Markdown_Cheat_Sheet_v1-1.pdf)
    ([local](aux/Markdown-2.pdf))

Atenció: existeixen moltes variants de Markdown, que afegeixen diferents
tipus d’ampliacions (com ara la utilitzada a
[GitHUB](https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet));
nosaltres treballarem tan sols la versió original, la més simple de
totes.

Per convertir fitxers Markdown a XHTML existeix moltes eines: pots usar
per exemple l’ordre `markdown(1)` que hi ha en el paquet RPM
[Discount](http://www.pell.portland.or.us/~orc/Code/discount/).

Una important influència en el disseny de Markdown la va proporcionar
[Aaron Swartz](http://www.aaronsw.com/weblog/001189), famós hacker i
[activista](http://www.aaronsw.com/) per les llibertats a Internet.

Pràctiques
----------

1.  -   Instal·la si cal l’XML Copy Editor.
    -   Consulta l’ajuda interactiva de l’editor i la seva pàgina web.
    -   Instal·la el paquet `aspell` i paquets auxiliars per poder
        revisar ortografia amb l’editor XML Copy Editor o directament en
        el terminal.
    -   Fes amb XML Copy Editor una pàgina XHTML sobre el tema “*El
        Cultivo del Berberecho*” (busca informació per Internet sobre
        aquest tema).

2.  -   Instal·la el paquet `geany-plugins-markdown`.
    -   Converteix la pàgina que has fet sobre el *berberecho* a
        Markdown amb un editor convencional com Geany. No oblidis
        revisar l’ortografia amb `aspell`.
    -   Explora el *plugin/add-on* de Firefox per editar correu
        Markdown Here.


